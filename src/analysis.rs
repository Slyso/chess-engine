use Direction::*;

use crate::board::Board;
use crate::castle::possibilities_castling;
use crate::direction::Direction;
use crate::piece::Piece::*;
use crate::player::Player;
use crate::position::Position;
use crate::square::Square;

type State = (Board, Position);

pub fn all_possibilities(source: &Square, board: &Board) -> Vec<State> {
    let even_directions = [North, East, South, West];
    let odd_directions = [NorthEast, SouthEast, SouthWest, NorthWest];
    let all_directions = &[even_directions, odd_directions].concat();

    match source.get_piece() {
        Rook => {
            possibilities_for_directions(&even_directions, None, true, source, board)
        }
        Bishop => {
            possibilities_for_directions(&odd_directions, None, true, source, board)
        }
        Queen => {
            possibilities_for_directions(all_directions, None, true, source, board)
        }
        King => {
            let mut possibilities = possibilities_for_directions(all_directions, Some(1), true, source, board);
            possibilities.append(&mut possibilities_castling(source, board));
            possibilities
        }
        Pawn => possibilities_for_pawn(source, board),
        Knight => possibilities_for_knight(source, board)
    }
}

pub fn is_valid_move(source: &Square, target: &Position, board: &Board) -> bool {
    all_possibilities(source, board)
        .iter()
        .any(|(_, position)| position == target)
}

fn possibilities_for_directions(directions: &[Direction], distance_limit: Option<u8>, can_capture: bool,
                                source: &Square, board: &Board) -> Vec<State> {
    let mut states: Vec<State> = vec![];
    let limit = distance_limit.unwrap_or(7);

    for direction in directions {
        let mut position = source.get_position();
        for _ in 0..limit {
            position.change(1, direction);
            if !position.is_in_bounds() { break; }

            let mut board = board.clone();
            let target = board.at(position);
            let mut piece_captured = false;

            if !target.is_empty() {
                if !can_capture || target.get_owner() == source.get_owner() { break; }
                piece_captured = true;
            }

            board.move_piece(source.get_position(), position);
            states.push((board, position));

            if piece_captured { break; }
        }
    }

    states
}

fn possibilities_for_pawn(source: &Square, board: &Board) -> Vec<State> {
    let mut states = vec![];
    let direction = if source.get_owner() == Player::White { &North } else { &South };
    let in_starting_position =
        source.get_owner() == Player::White && source.get_position().rank == 1 ||
            source.get_owner() == Player::Black && source.get_position().rank == 6;

    let limit = if in_starting_position { 2 } else { 1 };

    states.append(&mut possibilities_for_directions(
        &[*direction], Some(limit), false, source, board));

    let mut left_capture_position = source.get_position();
    let mut right_capture_position = source.get_position();

    if direction == &North {
        left_capture_position.change(1, &NorthWest);
        right_capture_position.change(1, &NorthEast);
    } else {
        left_capture_position.change(1, &SouthWest);
        right_capture_position.change(1, &SouthEast);
    }

    let mut check_capture_position = |position: Position| {
        let target = board.at(position);

        if !target.is_empty() && target.get_owner() != source.get_owner() {
            let mut board = board.clone();
            board.move_piece(source.get_position(), position);
            states.push((board, position));
        }
    };

    if left_capture_position.is_in_bounds() { check_capture_position(left_capture_position) }
    if right_capture_position.is_in_bounds() { check_capture_position(right_capture_position) }

    states
}

fn possibilities_for_knight(source: &Square, board: &Board) -> Vec<State> {
    let source_position = source.get_position();
    let rank = source_position.rank;
    let file = source_position.file;

    (0..8).map(|i: u8| {
        let mut file_delta = if i < 4 { 2 } else { 1 };
        let mut rank_delta = if i < 4 { 1 } else { 2 };

        if (i / 2) % 2 == 1 { file_delta *= -1 }
        if i % 2 == 1 { rank_delta *= -1 }
        Position::new(file + file_delta, rank + rank_delta)
    })
        .filter(|position| position.is_in_bounds())
        .filter(|position| {
            let target = board.at(*position);
            target.is_empty() || target.get_owner() != source.get_owner()
        })
        .map(|position| {
            let mut board = board.clone();
            board.move_piece(source_position, position);
            (board, position)
        }).collect::<Vec<_>>()
}

