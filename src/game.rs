use crate::board::Board;
use crate::position::Position;
use crate::analysis;
use crate::player::Player;

pub struct Game {
    history: Tree<(Board, Player)>,
    current_state: NodeId,
}

impl Game {
    pub fn new(board: Board, turn: Player) -> Self {
        let history = Tree::new((board, turn));
        let root = history.root();
        Game { history, current_state: root }
    }

    pub fn get_state(&self) -> &(Board, Player) {
        &self.history.get(self.current_state).data
    }

    pub fn move_piece(&mut self, from: Position, to: Position) -> Result<(), &'static str> {
        let (board, turn) = self.get_state();

        if !self.is_valid_source(from) {
            return Err("Invalid source");
        }

        if !analysis::is_valid_move(board.at(from), &to, board) {
            return Err("Invalid move");
        }

        let mut new_board = board.clone();
        new_board.move_piece(from, to);

        let new_state = (new_board, turn.opponent());
        self.current_state = self.history.add_child(self.current_state, new_state);

        Ok(())
    }

    pub fn is_valid_source(&self, source: Position) -> bool {
        let (board, turn) = self.get_state();
        let square = board.at(source);

        !square.is_empty() && &square.get_owner() == turn
    }

    pub fn undo_move(&mut self) {
        if let Some(parent) = self.history.get(self.current_state).parent {
            self.current_state = parent;
        }
    }

    pub fn redo_move(&mut self, state: Option<usize>) -> Result<(), &'static str> {
        let children = &self.history.get(self.current_state).children;

        if children.is_empty() { return Err("No future state exists"); }

        match state {
            None => {
                if children.len() > 1 {
                    return Err("Multiple future states exist, please specify the state");
                }

                self.current_state = children[0];
                Ok(())
            }
            Some(number) => {
                if number >= children.len() {
                    return Err("State number too big");
                }

                self.current_state = children[number];
                Ok(())
            }
        }
    }
}

struct Tree<T> {
    nodes: Vec<Node<T>>,
}

impl<T> Tree<T> {
    fn new(data: T) -> Self {
        let root = Node { parent: None, children: vec![], data };
        Tree { nodes: vec![root] }
    }

    fn root(&self) -> NodeId {
        NodeId { id: 0 }
    }

    fn get(&self, id: NodeId) -> &Node<T> {
        &self.nodes[id.id]
    }

    fn add_child(&mut self, parent: NodeId, data: T) -> NodeId {
        let id = NodeId { id: self.nodes.len() };
        self.nodes[parent.id].children.push(id);

        self.nodes.push(Node { parent: Some(parent), children: vec![], data });
        id
    }
}

struct Node<T> {
    parent: Option<NodeId>,
    children: Vec<NodeId>,
    data: T,
}

#[derive(Copy, Clone)]
struct NodeId {
    id: usize,
}