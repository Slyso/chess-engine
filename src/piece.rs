#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub enum Piece {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

use Piece::*;

impl Piece {
    pub fn relative_value(&self) -> i8 {
        match self {
            Pawn => 1,
            Knight => 3,
            Bishop => 3,
            Rook => 5,
            Queen => 9,
            King => 50,
        }
    }
}
