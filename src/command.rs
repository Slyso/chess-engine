use std::convert::TryFrom;
use std::ops::RangeInclusive;
use std::process::exit;
use std::str::FromStr;

use rand::seq::SliceRandom;
use rand::thread_rng;

use crate::algorithm;
use crate::game::Game;
use crate::position::Position;

macro_rules! default_depth { () => ( 5 ) }

pub trait Command {
    fn command(&self) -> &'static str;
    fn argument_count(&self) -> RangeInclusive<usize>;
    fn description(&self) -> &'static str;
    fn run(&self, arguments: &[&str], game: &mut Game);
}

struct Board {}

impl Command for Board {
    fn command(&self) -> &'static str { "board" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=0 }
    fn description(&self) -> &'static str { "Display the chess board in the current state" }

    fn run(&self, _: &[&str], game: &mut Game) {
        println!("{}", game.get_state().0);
    }
}

struct Undo {}

impl Command for Undo {
    fn command(&self) -> &'static str { "undo" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=0 }
    fn description(&self) -> &'static str { "Go one step up in the history tree" }

    fn run(&self, _: &[&str], game: &mut Game) {
        game.undo_move();
    }
}

struct Redo {}

impl Command for Redo {
    fn command(&self) -> &'static str { "redo" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=1 }
    fn description(&self) -> &'static str { "Go one step down in the history tree" }

    fn run(&self, arguments: &[&str], game: &mut Game) {
        let result = match arguments.len() {
            0 => game.redo_move(None),
            1 => {
                match usize::from_str(arguments[0]) {
                    Ok(x) => game.redo_move(Some(x)),
                    Err(_) => {
                        println!("Expected a positive number as argument");
                        return;
                    }
                }
            }
            _ => unreachable!()
        };

        if let Err(message) = result {
            println!("{}", message);
        }
    }
}

struct Help {}

impl Command for Help {
    fn command(&self) -> &'static str { "help" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=0 }
    fn description(&self) -> &'static str { "Show a list of available commands" }

    fn run(&self, _: &[&str], _: &mut Game) {
        for command in get_commands() {
            println!("{}  {}", command.command(), command.description());
        }
    }
}

struct Move {}

impl Command for Move {
    fn command(&self) -> &'static str { "move" }
    fn argument_count(&self) -> RangeInclusive<usize> { 2..=2 }
    fn description(&self) -> &'static str {
        "Move a piece on the board by providing two arguments. \
         First the source then the destination square. \
         Example: move e2 e4"
    }

    fn run(&self, arguments: &[&str], game: &mut Game) {
        let from = Position::try_from(arguments[0]);
        let to = Position::try_from(arguments[1]);

        if from.is_err() {
            println!("{}", from.unwrap_err());
            return;
        }

        if to.is_err() {
            println!("{}", to.unwrap_err());
            return;
        }

        match game.move_piece(from.unwrap(), to.unwrap()) {
            Ok(_) => Computer {}.run(&[], game),
            Err(message) => println!("{}", message),
        }
    }
}

struct Computer {}

impl Command for Computer {
    fn command(&self) -> &'static str { "computer" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=1 }
    fn description(&self) -> &'static str {
        concat!("Force the computer to make a move. \
                 Try to find the 'best' move looking n moves ahead. \
                 n is called the depth. The default depth is ", default_depth!())
    }

    fn run(&self, arguments: &[&str], game: &mut Game) {
        let (board, turn) = game.get_state();

        let depth = match arguments.len() {
            0 => default_depth!(),
            1 => {
                match u8::from_str(arguments[0]) {
                    Ok(depth) => depth,
                    Err(_) => {
                        println!("Expected positive number");
                        return;
                    }
                }
            }
            _ => unreachable!()
        };

        if depth == 0 || depth > 10 {
            println!("Please choose a depth from 1 to 10");
            return;
        }

        match algorithm::best_moves(board, *turn, depth).choose(&mut thread_rng()) {
            None => println!("No move found"),
            Some((from, to)) => {
                game.move_piece(*from, *to).unwrap();
                println!("Moved from {} to {}", from, to);
            }
        }
    }
}

struct Exit {}

impl Command for Exit {
    fn command(&self) -> &'static str { "exit" }
    fn argument_count(&self) -> RangeInclusive<usize> { 0..=0 }
    fn description(&self) -> &'static str { "Terminate the program" }

    fn run(&self, _: &[&str], _: &mut Game) {
        exit(0);
    }
}

pub fn valid_argument_count(count: &usize, command: &dyn Command) -> bool {
    command.argument_count().contains(count)
}

pub fn get_commands() -> Vec<Box<dyn Command>> {
    vec![
        Box::new(Board {}),
        Box::new(Undo {}),
        Box::new(Redo {}),
        Box::new(Help {}),
        Box::new(Move {}),
        Box::new(Exit {}),
        Box::new(Computer {}),
    ]
}
